﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public class Program
    {
        public static IList<IMalpa> TworzenieListy()
        {
            List<IMalpa> Malpy = new List<IMalpa>() { new Impl1(), new Impl2(), new Impl3() };
            return (IList<IMalpa>)Malpy;
        }

        public static void Wypisywanie(IList<IMalpa> Malpy)
        {
            foreach (IMalpa malpa in Malpy)
            {
                malpa.Jedz();
            }
        }

        static void Main(string[] args)
        {

            Impl3 test3 = new Impl3();

            ((IMalpa)test3).Jedz();
            ((IOlbrzym)test3).Jedz();
            test3.Jedz();

            Console.ReadKey();
        }
    }
}
