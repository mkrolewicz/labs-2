﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    class Impl1 : IGoryl
    {
        public Impl1()
        { }
        public string Jedz()
        {
            return "Goryl je";
        }
        public void Biegnij()
        {
            Console.WriteLine("Goryl biegnie");
        }
    }

    class Impl2 : IPawian
    {
        public Impl2()
        { }
        public string Jedz()
        {
            return "Pawian Je";
        }
        public void Skacz()
        {
            Console.WriteLine("Pawian skacze");
        }
    }
    public class Impl3 : IGoryl, IOlbrzym
    {
        public Impl3()
        { }

        public string Jedz()
        {
            return "Impl3: Jem";
        }
        string IMalpa.Jedz()
        {
            return "IMalpa: Jem";
        }
        string IOlbrzym.Jedz()
        {
            return "IOlbrzym: Jem";
        }
        public void Biegnij()
        {
            Console.WriteLine("Gorylo-Olbrzym biegnie");
        }
        public void Zmniejsz()
        {
            Console.WriteLine("Gorylo-Olbrzym zmniejsza sie");
        }
    }

}
