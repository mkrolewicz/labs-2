﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Contract
{
    public interface IMalpa
    {
        string Jedz();
    }
    public interface IGoryl : IMalpa
    {
        void Biegnij();
    }
    public interface IPawian : IMalpa
    {
        void Skacz();
    }
    public interface IOlbrzym 
    {
        string Jedz();
        void Zmniejsz();
    }

}
